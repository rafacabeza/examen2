{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2>Alta de estudios</h2>
    
    <form action="{$url}{$lang}/study/update" method="post">
        <label>id</label><input type="text" name="id" value="{$row.id}"><br>
        <label>Codigo Interno</label><input type="text" name="codinterno" value="{$row.codInterno}"><br>
        <label>Nombre</label><input type="text" name="nombre" value="{$row.nombre}"><br>
        <label>Nivel</label>
        
{*        <input type="text" value="{$row.idNivel}">  
*}        
        <select name="idNivel" >
{*            <option value=0>{$language->translate('select_one')}</option>*}
            {foreach $levelRows as $levelRow}
            <option {($row.idNivel==$levelRow.id)? 'selected' : ''} 
                value="{$levelRow.id}">
                {$levelRow.nivel}
            </option>
            {/foreach}
        </select> 

        <br>
        <label>Codigo Oficial</label><input type="text" name="codOficial" value="{$row.codOficial}"><br>
        <label></label><input type="submit" value="Enviar">
    </form>

 </div>
{include file="template/footer.tpl" title="footer"}