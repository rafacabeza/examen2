<?php

$en = array(
    //Aplicacion
    'app_title' => 'March Exam',
        
    //
    'operations' => 'Operations',
    'name' => 'Name',
    'user_list' => 'User list',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'new_user' => 'New user',
    'index' => 'Index',
    'help' => 'Help',
    'user'=> 'User',
    'error_password' => 'The password must be between 6 and 20 characters',
    //
    'study'=> 'Estudies',
    'study_list' => 'Studies list',
    'new_study' => 'New study',
    'innerCode' => 'Inner code',
    'officialCode' => 'Official code',
    'level' => 'Level',
    //controles select
    'select_one' => 'select one   ------------',
    //productos
    'product_list' => 'Product List',
    'new_product' => 'New product',
);