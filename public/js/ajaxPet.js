$(document).ready(function () {
    var deleteRecord = function (id) {
        url = 'http://localhost/examen2/pet/ajaxDelete/' + id;
        $.get(url, function (data) {
//            alert(JSON.toLocaleString(data));
            if (data) {
                $('#row' + id).empty();
            } else {
                alert('Operación denegada');
            }
            ;
        }, 'json');
    };

    var filterTable = function (text) {
//        alert(text);
        url = 'http://localhost/examen2/pet/ajaxGetDataFilter/';
        $.get(url, {filter: text}, function (data) {
//            alert (data[0].nombre); return;
            rows = data;
            $("#tbodyList").empty();
            for (var i = 0; i < rows.length; i++) {

                newRows = '<tr class="cell" id="row' + rows[i].id + '" idRow=' + rows[i].id + ' >';
                newRows = newRows + '<td > ' + rows[i].id + '</td>';
                newRows = newRows + '<td > ' + rows[i].nombre + '</td>';
                newRows = newRows + '<td > ' + rows[i].especie + '</td>';
                newRows = newRows + '<td > ' + rows[i].fecha + '</td>';
                newRows = newRows + '<td > <a id="delete' + rows[i].id + '" class="deleteRecord">Borrar</a> </td></tr>';


//                Toner Impresora X
                $("#tbodyList").append(newRows);
            }
            $(document).on('click', '.deleteRecord', function () {
                idRow = this.id;
                idRow = idRow.substring(6)
                deleteRecord(idRow);
            });
        }, 'json');
    };


    $(".deleteRecord").click(function () {
        idRow = this.id;
        idRow = idRow.substring(6)
        deleteRecord(idRow);
    });


    $("#filter").keyup(function () {
        filterTable($(this).val());
    });


    $(".petVisit").click(function () {
        miId =  $(this).attr('id');
        url = 'http://localhost/examen2/pet/ajaxAddPetList/';
        $.post(url, {
            id: miId ,
            nombre: 'b',
            especie: 'c'            
            }, 
            function (data) {
                alert (data);
        }, 'html');

    });

});